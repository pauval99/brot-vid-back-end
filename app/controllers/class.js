'use strict'

const service = require('../services')
const User = require('../models/user')
const Clas = require('../models/class')
const Room = require('../models/room')
const Subject = require('../models/subject')
const Schedule = require('../models/schedule')
const UserClass = require('../models/userclass')
const { ObjectId } = require('mongodb');
const { db } = require('../models/class')

function getTeachers(subjects) {
    var teachers = [];
    for (var i = 0; i < subjects.length; ++i) {
        if (!(subjects.teacher in teachers)) teachers.add(subjects.teacher);
    }
    return teachers;
}


function addUserClass(req, res) {
    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })

        User.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ message: err })
            if (!user) return res.status(404).send({ message: "no existe usuario" })
            if (user.role != "ALUMNO") return res.status(400).send({ message: "Solo se le pueden añadir clases a un alumno" })
            Subject.findOne({
                $and: [{
                    code: req.body.code
                }, {
                    name: req.body.name
                }]
            }, (err, subject) => {
                if (err) return res.status(500).send({ message: err })
                if (!subject) return res.status(404).send({ message: "no existe asignatura" })

                Schedule.findOne({
                    $and: [{
                        h_ini: req.body.h_ini
                    }, {
                        day: req.body.weekday
                    }]
                }, (err, schedule) => {
                    if (err) return res.status(500).send({ message: err })
                    if (!schedule) return res.status(404).send({ message: "no existe horario" })

                    Room.findOne({
                        $and: [{
                            floor: req.body.floor
                        }, {
                            number: req.body.number
                        }, {
                            building: req.body.building
                        }]
                    }, (err, room) => {
                        if (err) return res.status(500).send({ message: err })
                        if (!room) return res.status(404).send({ message: "no existe asignatura" })
                        if (req.body.building == "BIBLIOTECA") return res.status(403).send({ message: "No le puedes añadir una clase a la bibloteca" })
                        Clas.findOne({
                            $and: [{
                                room: room
                            }, {
                                schedule: schedule
                            }, {
                                subject: subject
                            }]
                        }, (err, clas) => {
                            if (err) return res.status(500).send({ message: err })
                            if (!clas) return res.status(404).send({ message: "no existe asignatura" })

                            let userclass = new UserClass({
                                user: user,
                                class: clas
                            })
                            userclass.save((err) => {
                                if (err) res.status(500).send({
                                    message: `Error al asignar clase ${err}`
                                })
                                Clas.findByIdAndUpdate({ _id: clas._id }, { num_students: clas.num_students + 1 }, (userUpdated) => {

                                })
                                return res.status(201).send({ message: "Se ha asignar la clase correctamente" })
                            })
                        })

                    })
                })
            })


        })
    })
}

function confined(req, res) {
    User.findById(req.user, (err, user) => {

        if (err) return res.status(500).send({ message: err })

        if (!user) return res.status(404).send({ message: "No existe usuario" })
        if (user.role == "ADMINISTRACION") return res.status(403).send({ message: "No eres alumno o profesor" })

        Clas.findById(req.params.id, (err, classe) => {
            if (err) return res.status(500).send({ message: err })
            if (!classe) return res.status(404).send({ message: "No existe la classe" })

            return res.status(200).send({ message: classe.num_confined })
        })
    })
}

function classes() {
    return db.collection("classes").find().toArray()
}

function getClasse(classe) {
    return db.collection("classes").find({ _id: ObjectId(classe) }, { projection: { subject: 1, schedule: 1, teacher: 1, room: 1, _id: 0 } }).toArray()
}

function getClasses(req, res) {
    classes().then((response) => {
        if (response != null) return res.status(200).send({ message: response })
        else return res.status(404).send({ message: "No hay classes" })
    })
}

module.exports = {
    getTeachers,
    addUserClass,
    confined,
    classes,
    getClasses,
    getClasse
}