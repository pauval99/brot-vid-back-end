'use strict'

const School = require('../models/school')

const { db } = require('../models/notification')
const User = require('../models/user')
const Notification = require('../models/notification')

function openSchool(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            School.findOneAndUpdate({ name: req.params.name }, { $set: { opened: true } }, { new: true }, (err, sch) => {
                var notification = new Notification({
                    creator: req.user,
                    creationDate: Date.now(),
                    title: "Centro " + req.params.name + " abierto",
                    description: "Se ha abierto el centro " + req.params.name,
                    type: "GENERAL",
                })
                notification.save()
                return res.status(201).send({ message: "Se ha añadido la apertura del centro" })
            })
        } else return res.status(403).send({ message: "No eres administrador" })
    })
}

function closeSchool(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            School.findOneAndUpdate({ name: req.params.name }, { $set: { opened: false } }, { new: true }, (err, sch) => {
                var notification = new Notification({
                    creator: req.user,
                    creationDate: Date.now(),
                    title: "Centro " + req.params.name + " cerrado",
                    description: "Se ha cerrado el centro " + req.params.name,
                    type: "GENERAL",
                })
                notification.save()
                return res.status(201).send({ message: "Se ha añadido el cierre del centro" })
            })
        } else return res.status(403).send({ message: "No eres administrador" })
    })
}

function getSchool(req, res) {
    School.findOne({ name: req.params.name }, (err, cent) => {
        if (cent) return res.status(200).send({ message: cent })
        else return res.status(404).send({ message: "No existe centro con ese nombre" })
    })

}

function createSchool(res) {
    var sc = new School({
        name: "FIB",
        opened: true
    })
    sc.save()
    if (sc) return res.status(200).send({ message: sc })
    else return res.status(404).send({ message: "No existe centro con ese nombre" })
}

module.exports = {
    openSchool,
    closeSchool,
    getSchool,
    createSchool
}