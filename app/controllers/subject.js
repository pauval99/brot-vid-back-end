'use strict'

const Subject = require('../models/subject')
const service = require('../services')
const { ObjectId } = require('mongodb');
const User = require('../models/user')
const UserSubject = require('../models/usersubject')
const { db } = require('../models/subject')

function createSubject(req, res) {

    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ADMINISTRACION") {
            return res.status(403).send({ message: "No tienes permiso para crear asignaturas" })
        } else {
            var subject = new Subject({
                code: req.body.code,
                name: req.body.name
            })
            subject.save((err) => {
                if (err) res.status(500).send({
                    message: `Error al crear la asignatura ${err}`
                })
                return res.status(201).send({ message: "Asignatura creada correctamente" })
            })
        }
    })
}

function addSubject(req, res) {

    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })
        User.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ message: err })
            if (!user) return res.status(404).send({ message: "no existe usuario" })
            if (user.role == "ADMINISTRACION") return res.status(400).send({ message: "no se pueden añadir asignaturas a administracion" })
            Subject.findOne({ code: req.body.code }, (err, subject) => {
                if (err) return res.status(500).send({ message: err })
                if (!subject) return res.status(404).send({ message: "no existe asignatura" })
                var teacher = false;
                if (user.role == "PROFESOR") teacher = true;
                let usersubject = new UserSubject({
                    user: user,
                    subject: subject,
                    teacher: teacher
                })
                usersubject.save((err) => {
                    if (err) res.status(500).send({
                        message: `Error al añadir asignatura ${err}`
                    })
                    return res.status(201).send({ message: "Se ha añadido la asignatura correctamente" })
                })
            })
        })
    })
}

function getSubject(subject_id) {
    return db.collection("subjects").find({ _id: ObjectId(subject_id) }, { projection: { name: 1, attendance: 1, _id: 0 } }).toArray()
}

module.exports = {
    createSubject,
    addSubject,
    getSubject
}