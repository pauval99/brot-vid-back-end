'use strict'

const Notification = require('../models/notification')

const { db } = require('../models/notification')
const User = require('../models/user')
const Subject = require('../models/subject')
const UserSubject = require('../controllers/usersubject')

function createNotification(usuario, titulo, descripcion, tipo) {
    var notification = new Notification({
        creator: usuario,
        creationDate: Date.now(),
        title: titulo,
        description: descripcion,
        type: tipo,
    })
    notification.save()
    return notification
}

function getNotification(id) {
    return db.collection("notifications")
        .find({ _id: id })
        .toArray()
}

function getNotifications(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            var notifi = db.collection('notifications');
            notifi.find().toArray((err, items) => {
                if (err) return res.status(500).send({ message: err })
                if (!items) return res.status(404).send({ message: "No hay notificaciones que mostrar." })
                return res.status(200).send(items);
            })
        } else return res.status(403).send({ message: "No tienes permisos" });
    })
}


function getNotificationsFiltred(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            var notifi = db.collection('notifications');
            notifi.find({ type: req.params.type }).toArray((err, items) => {
                if (err) return res.status(500).send({ message: err })
                if (!items) return res.status(404).send({ message: "No hay notificaciones que mostrar." })
                return res.status(200).send(items);
            })
        } else return res.status(403).send({ message: "No tienes permisos" });
    })
}

function createNews(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            var notification = new Notification({
                creator: req.user,
                creationDate: Date.now(),
                title: req.body.title,
                description: req.body.descripcion,
                type: "NOTICIAS",
            })
            notification.save()
            return res.status(201).send({ message: "Se ha añadido correctamente la noticia" })
        } else return res.status(403).send({ message: "No eres administrador" })
    })
}

function createSecurityMeasures(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            var notification = new Notification({
                creator: req.user,
                creationDate: Date.now(),
                title: req.body.title,
                description: req.body.descripcion,
                type: "SEGURIDAD",
            })
            notification.save()
            return res.status(201).send({ message: "Se ha añadido correctamente la noticia" })
        } else return res.status(403).send({ message: "No eres administrador" })
    })
}

function addRegulations(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            var notification = new Notification({
                creator: req.user,
                creationDate: Date.now(),
                title: req.body.title,
                description: req.body.descripcion,
                type: "NORMATIVA",
            })
            notification.save()
            return res.status(201).send({ message: "Se ha añadido nueva normativa" })
        } else return res.status(403).send({ message: "No eres administrador" })
    })
}

function addIncidences(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ADMINISTRACION") {
            var notification = new Notification({
                creator: req.user,
                creationDate: Date.now(),
                title: req.body.title,
                description: req.body.descripcion,
                type: "INCIDENCIA",
            })
            notification.save()
            return res.status(201).send({ message: "Se ha añadido nueva incidencia" })
        } else return res.status(403).send({ message: "No eres ni profesor ni alumno" })
    })
}

function resolveIncidences(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            Notification.findByIdAndDelete({ _id: req.params.id }, (err, not) => {
                if (err) res.status(500).send(`${err}`)
                else if (not) return res.status(201).send({ message: "Se ha resuelto la incidencia" })
            })
        } else return res.status(403).send({ message: "No eres ni profesor ni alumno" })
    })
}

module.exports = {
    createNotification,
    getNotifications,
    getNotificationsFiltred,
    getNotification,
    createNews,
    createSecurityMeasures,
    addRegulations,
    addIncidences,
    resolveIncidences
}