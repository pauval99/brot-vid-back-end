'use strict'
const { ObjectId } = require('mongodb')
const User = require('../models/user')
const Subject = require('../models/subject')
const UserSubject = require('../controllers/usersubject')
const UserNotification = require('../models/usernotification')
const ctrlNotification = require('../controllers/notification')
const Notification = require('../models/notification')

function getUserNotifications(req, res) {
    let notifications = []
    let others = []
    Notification.find({ type: { $in: ["GENERAL", "NOTICIAS", "SEGURIDAD", "NORMATIVA", "INCIDENCIA"] } }, (err, not) => {
        if (err) return res.status(400).send({ err })
        notifications = not
    })
    var j = 0
    UserNotification.find({ user: ObjectId(req.user) }, (err, notifi) => {
        if (err) return res.status(400).send({ message: err })
        if (notifi.length == 0 && notifications.length == 0) return res.status(404).send({ message: "No hay notificaciones que mostrar" })
        if (notifi.length == 0 && notifications.length != 0) return res.send({ message: notifications })

        for (var i = 0; i < notifi.length; ++i) {
            ctrlNotification.getNotification(notifi[i].notification).then((response) => {
                others = response
                notifications = notifications.concat(others)
                    ++j
                if (j == notifi.length) return res.send({ message: notifications })
            })
        }
    })
}

function assignNotification(user, not) {
    var usernotification = new UserNotification({
        user: user,
        notification: not
    })
    usernotification.save()
}


function changeAssistanceTypeF2F(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) res.status(500).send({ message: `${err}` })
        if (!user) res.status(404).send({ message: "El usuario no existe" })
        if (user.role == "PROFESOR" || user.role == "ADMINISTRACION") {
            Subject.findOneAndUpdate({ code: req.params.subject }, { attendance: false }, (err, subjUpdated) => {
                if (err) return res.status(500).send({ message: `Error al modificar tipo de assistencia ${err}` });
                if (!subjUpdated) res.status(404).send({ message: "El usuario no existe" })
                else {
                    UserSubject.getSubjectUsers(subjUpdated._id).then((response) => {

                        if (response.length == 0) return res.status(404).send({ message: "No hay alumnos matriculados" })

                        var not = ctrlNotification.createNotification(req.user, "Cambio de asistencia de " + subjUpdated.name, "La asignatura " + subjUpdated.name + " ha cambiado su tipo de asistencia a online", "ASISTENCIA")
                        for (var i = 0; i < response.length; ++i) {
                            assignNotification(response[i].user, not)
                        }
                        return res.status(200).send({ message: "Se ha notificado correctamente el cambio de asistencia" })
                    })
                }
            })
        }
    })
}

function changeAssistanceTypeOnline(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) res.status(500).send({ message: `${err}` })
        if (!user) res.status(404).send({ message: "El usuario no existe" })
        if (user.role == "PROFESOR" || user.role == "ADMINISTRACION") {
            Subject.findOneAndUpdate({ code: req.params.subject }, { attendance: true }, (err, subjUpdated) => {
                if (err) return res.status(500).send({ message: `Error al modificar tipo de assistencia ${err}` });
                else {
                    UserSubject.getSubjectUsers(subjUpdated._id).then((response) => {

                        if (response.length == 0) return res.status(404).send({ message: "No hay alumnos matriculados" })

                        var not = ctrlNotification.createNotification(req.user, "Cambio de asistencia de " + subjUpdated.name, "La asignatura " + subjUpdated.name + " ha cambiado su tipo de asistencia a presencial", "ASISTENCIA")
                        for (var i = 0; i < response.length; ++i) {
                            assignNotification(response[i].user, not)
                        }
                        return res.status(200).send({ message: "Se ha notificado correctamente el cambio de asistencia" })
                    })
                }
            })
        }
    })
}

function changeDocentGuide(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) res.status(500).send({ message: `${err}` })
        if (!user) res.status(404).send({ message: "El usuario no existe" })
        if (user.role == "PROFESOR" || user.role == "ADMINISTRACION") {
            Subject.findOne({ code: req.params.subject }, (err, subj) => {
                if (err) return res.status(500).send({ message: `${err}` })
                if (!subj) return res.status(404).send({ message: "No existe asignatura" })
                UserSubject.getSubjectUsers(subj._id).then((response) => {
                    if (response.length == 0) return res.status(404).send({ message: "No hay alumnos matriculados" })
                    var not = ctrlNotification.createNotification(req.user, "Cambio en la guia docente de " + subj.name, "La asignatura " + subj.name + " ha cambiado su guía docente", "GUIA")
                    for (var i = 0; i < response.length; ++i) {
                        assignNotification(response[i].user, not)
                    }
                    return res.status(200).send({
                        message: "Se ha notificado correctamente el cambio en la guia docente"
                    })

                })
            })
        }
    })
}


module.exports = {
    getUserNotifications,
    assignNotification,
    changeAssistanceTypeF2F,
    changeAssistanceTypeOnline,
    changeDocentGuide
}