'use strict'

const { response } = require('express');
const { ObjectId } = require('mongodb');
const user = require('../models/user');
const Subject = require('../models/subject');
const { db } = require('../models/usersubject');
const usersubject = require('../models/usersubject');
const Class = require('../models/class');
const room = require('../models/room');

function getUserSubjects(user) {
    return db.collection("usersubjects")
        .find({ user: ObjectId(user) }, { projection: { subject: 1, _id: 0 } })
        .toArray()
}

function getSubjectsTeacher(subject) {
    return db.collection("usersubjects")
        .find({ subject: ObjectId(subject), teacher: true }, { projection: { user: 1, _id: 0 } })
        .toArray()
}

function getSubjectUsers(subject) {
    return db.collection("usersubjects")
        .find({ subject: ObjectId(subject) }, { projection: { user: 1, _id: 0 } })
        .toArray()
}

function getTeacherSubjects(req, res) {
    user.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "PROFESOR") return res.status(403).send({ message: "No tienes permisos" })
        Class.find({teacher: ObjectId(user._id)}).select('subject room schedule -_id').populate([
            {
                path: 'subject',
                model: 'Subject',
                select: 'name attendance code -_id'
            },
            {
                path: 'room',
                model: 'Room',
                select: 'floor number building -_id',
            },
            {
                path: 'schedule',
                model: 'Schedule',
                select: 'h_ini h_end day -_id',
            }
        ]).then((subjs) => {
            return res.status(200).send(subjs);
        })
    })
}

module.exports = {
    getUserSubjects,
    getSubjectsTeacher,
    getSubjectUsers,
    getTeacherSubjects
}