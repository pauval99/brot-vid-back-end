'use strict'

const Chair = require('../models/chair')
const { ObjectId } = require('mongodb');
const service = require('../services')
const User = require('../models/user')
const { db } = require('../models/chair');


function createChair(classroom) {

    for (let i = 0; i < classroom.capacity; i++) {
        let chair = new Chair({
            QR: classroom.floor.toString() + classroom.number.toString() + i + classroom.building,
            room: classroom,
            number: i
        })

        chair.save((err) => {
            if (err) return true
            return false
        })
    }
}

function getQR(chair) {
    return db.collection("chairs").find({ _id: ObjectId(chair) }, { projection: { QR: 1, _id: 0 } }).toArray()
}

function getChairsFromRoom(room) {
    return db.collection("chairs").find({ room: ObjectId(room._id) })
        .toArray()
}


module.exports = {
    createChair,
    getChairsFromRoom,
    getQR
}