'use strict'


const Room = require('../models/room');
const User = require('../models/user');
const ctrlNotification = require('./notification')
const ctrlChair = require('./chair')
const ctrlBiblioSeat = require('./biblioseat')
const { ObjectId } = require('mongodb');
const { db } = require('../models/room');

function closeRoom(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            Room.findOneAndUpdate({ floor: req.body.floor, number: req.body.number, building: req.body.building }, { closed: true }, (err, roomUpdated) => {
                if (err) {
                    res
                        .status(500)
                        .send({ message: "Error al cerrar aula" });
                }
                if (!roomUpdated) return res.status(404).send({ message: "No existe aula" })
                else {
                    if (roomUpdated.closed) return res.status(400).send({ message: "El aula : " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + " ya está cerrada" })
                    var titulo = "Aula " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + " cerrada."
                    var description = "Se ha cerrado el aula " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + "."
                    var tipo = "GENERAL"
                    ctrlNotification.createNotification(user.displayName, titulo, description, tipo)
                    return res.status(200).send({ message: "Se ha cerrado el aula correctamente" })

                }
            })
        }
    })
}

function availableRooms() {
    return db.collection("rooms").find({ available: true })
        .toArray()
}

function rooms() {
    return db.collection("rooms").find().toArray()
}

function getRooms(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            rooms().then((response) => {
                if (response != null) return res.status(200).send({ message: response })
                else return res.status(404).send({ message: "No hay aulas" })
            })
        } else return res.status(403).send({ message: "No estas logueado como admin" })
    })
}

function getAvailableRooms(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ALUMNO") {
            availableRooms().then((response) => {
                if (response != null) return res.status(200).send({ message: response })
                else return res.status(404).send({ message: "No hay aulas disponibles" })
            })
        } else return res.status(403).send({ message: "No estas logueado como alumno" })
    })
}

function openRoom(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            Room.findOneAndUpdate({ floor: req.body.floor, number: req.body.number, building: req.body.building }, { closed: false }, (err, roomUpdated) => {
                if (err) {
                    res
                        .status(500)
                        .send({ message: "Error al abrir aula" });
                }
                if (!roomUpdated) return res.status(404).send({ message: "No existe aula" })
                else {
                    if (!roomUpdated.closed) return res.status(400).send({ message: "El aula : " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + " ya est� abierta" })
                    var titulo = "Aula " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + " abierta."
                    var description = "Se ha abierto el aula " + roomUpdated.building + roomUpdated.floor + roomUpdated.number + "."
                    var tipo = "GENERAL"
                    ctrlNotification.createNotification(user.displayName, titulo, description, tipo)
                    return res.status(200).send({ message: "Se ha abierto el aula correctamente" })

                }
            })
        }
    })
}

function getRoomFromFloor(floornumber) {
    return db.collection("rooms").find({ floor: floornumber, building: "BIBLIOTECA" }).toArray()
}

function consultFreeChairs(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ALUMNO")
            return res.status(400).send({
                message: "El usuario logueado no es un alumno"
            })
        getRoomFromFloor(Number(req.query.floor)).then((response) => {
            var j = 0
            ctrlChair.getChairsFromRoom(response[0]).then((chairs) => {
                var cont = 0
                for (var i = 0; i < chairs.length; ++i) {
                    ctrlBiblioSeat.isAvailable(chairs[i]).then((available) => {
                        console.log(available)
                        if (available) {
                            cont = cont + 1
                        }
                        if (j == (chairs.length - 1)) {
                            return res.status(200).send({ message: cont })
                        }
                        ++j
                    })
                }
                if (!chairs) return res.status(404).send({ message: "No se han encontrado sillas en esta planta" })
            })
            if (!response) return res.status(404).send({ message: "No existe esta planta" })
        })

    })
}

function getRoom(room) {
    return db.collection("rooms").find({ _id: ObjectId(room) }, { projection: { floor: 1, number: 1, building: 1, _id: 0 } }).toArray()
}
module.exports = {
    closeRoom,
    openRoom,
    getAvailableRooms,
    getRooms,
    consultFreeChairs,
    getRoom
}