'use strict'

const { response } = require('express');
const { ObjectId } = require('mongodb');
const { db } = require('../models/userclass');
const Clas = require('../models/class')

function getUserClasses(user) {
    return db.collection("userclasses")
    .find({ user: ObjectId(user._id) }, { projection: { class: 1, _id: 0 } })
    .toArray()
}

function addPositive(user){
    console.log(user)
    getUserClasses(user).then((response) => {
        console.log(response)
        for (var i = 0; i < response.length; ++i) {
            console.log(response[i].class)
            Clas.findByIdAndUpdate({_id: response[i].class}, {$inc: { num_infected: 1 }}, (err, classUpdated) => {
                if (err) res.status(500).send({ message: "Error al añadir positivo" });
                console.log(classUpdated)
            })
        }
    })
}

function addNegative(user){
    console.log(user)
    getUserClasses(user).then((response) => {
        console.log(response)
        for (var i = 0; i < response.length; ++i) {
            console.log(response[i].class)
            Clas.findByIdAndUpdate({_id: response[i].class},  {$inc: { num_infected: -1 }}, (err, classUpdated) => {
                if (err) res.status(500).send({ message: "Error al añadir negativo" });
                console.log(classUpdated)
            })
        }
    })
}

function addConfined(user){
    getUserClasses(user).then((response) => {
        for (var i = 0; i < response.length; ++i) {
            Clas.findByIdAndUpdate({_id: response[i].class},  {$inc: { num_confined: +1 }}, (err, classUpdated) => {
                if (err) res.status(500).send({ message: "Error al añadir negativo" });
                console.log(classUpdated)
            })
        }
    })
}

function addDeconfined(user){
    getUserClasses(user).then((response) => {
        for (var i = 0; i < response.length; ++i) {
            Clas.findByIdAndUpdate({_id: response[i].class},  {$inc: { num_confined: -1 }}, (err, classUpdated) => {
                if (err) res.status(500).send({ message: "Error al añadir negativo" });
                console.log(classUpdated)
            })
        }
    })
}




module.exports = {
    getUserClasses,
    addPositive,
    addNegative,
    addConfined,
    addDeconfined
}