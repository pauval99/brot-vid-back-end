'use strict'

const Schedule = require('../models/schedule')
const { ObjectId } = require('mongodb');
const service = require('../services')
const { db } = require('../models/schedule')

function getSchedule(schedule_id) {
    return db.collection("schedules").find({ _id: ObjectId(schedule_id)}, { projection: {h_ini: 1, h_end: 1, _id: 0 }}).toArray()
}

module.exports = {
    getSchedule
}