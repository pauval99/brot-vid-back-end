'use strict'

const service = require('../services')
const bcrypt = require('bcrypt')
const Subject = require('../models/subject')
const User = require('../models/user')
const ctrlUserSubject = require('./usersubject')
const ctrlNotification = require('./notification')
const ctrlUserNotification = require('../controllers/usernotification')
const ctrlUserClass = require('../controllers/userclass')
const ctrlBiblioSeat = require('../controllers/biblioseat')
const ctrlSubject = require('../controllers/subject')
const StudentSubject = require('../models/usersubject')
const Chair = require('../models/chair')
const Class = require('../models/class')
const Schedule = require('../models/schedule')
const Room = require('../models/room')
const Seat = require('../models/seat')
const BiblioSeat = require('../models/biblioseat')
const chairController = require('./chair')
const moment = require('moment')
const { ObjectId } = require('mongodb');
const { db } = require('../models/user')
const userclass = require('../models/userclass')

function signUp(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION") {
            let user = new User({
                email: req.body.email,
                displayName: req.body.displayName,
                password: bcrypt.hashSync(req.body.password, 10),
                role: req.body.role,
                confined: false,
                infected: false,
            })
            user.save((err) => {
                if (err) res.status(500).send({
                    message: `Error al crear el usuario ${err}`
                })
                return res.status(201).send({ token: service.createToken(user) })
            })
        } else if (user.role != "ADMINISTRACION") return res.status(403).send({ message: "No puedes crear usuarios" })
    })
}

function signIn(req, res) {

    User.findOne({ email: req.body.email }, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION")
            return res.status(400).send({
                message: "El usuario logueado no es ni alumno ni profesor"
            })
        if (!bcrypt.compareSync(req.body.password, user.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario o contraseña incorrectos"
                }
            });
        }
        req.user = user
        res.status(200).send({
            message: "Te has logueado correctamente",
            token: service.createToken(user),
            displayName: user.displayName,
            role: user.role

        })
    });
}

function getUser(req, res) {
    User.findById(req.params.id, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        return res.status(200).send({ message: user })
    })
}

function getUsers(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: `${err}` })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ADMINISTRACION" && user.role != "PROFESOR")
            return res.status(403).send({
                message: "El usuario logueado no es administrador o profesor"
            })
        else {
            User.find((err, users) => {
                if (err) return res.status(500).send({ message: `${err}` })
                if (!users) return res.status(404).send({ message: "No existen alumnos" })
                return res.status(200).send({ message: users })
            })
        }
    })
}



function home(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ALUMNO") {
            //si eres alumno lo que necesitas ver
            return res.status(200).send({ message: "Perfil de alumno" })
        } else if (user.role == "PROFESOR") {
            //si eres profe lo que necesitas ver
            return res.status(200).send({ message: "Perfil de profesor" })
        } else {
            //si eres admin lo que necesitas ver
            return res.status(200).send({ message: "Perfil de administracion" })
        }
    });
}

function addPositive(req, res) {
    User.findByIdAndUpdate({ _id: req.user }, { infected: true, confined: true }, (err, userUpdated) => {
        if (err) {
            res
                .status(500)
                .send({ message: "Error al añadir positivo" });
        } else {
            if (userUpdated.infected) return res.status(400).send({ message: "El usuario: " + userUpdated.displayName + " tiene un test positivo" })
            var titulo = "Usuario positivo."
            var description = "El usuario " + userUpdated.displayName + " ha añadido un test positivo."
            var tipo = "POSITIVO"
            var not = ctrlNotification.createNotification(userUpdated.displayName, titulo, description, tipo)
            ctrlUserSubject.getUserSubjects(req.user).then((response) => {
                    let subjects = [];
                    for (var i = 0; i < response.length; ++i) {
                        subjects.push(response[i].subject)
                    }
                    for (var i = 0; i < subjects.length; ++i) {
                        ctrlUserSubject.getSubjectsTeacher(subjects[i]).then((response) => {
                            for (var j = 0; j < response.length; ++j) {
                                ctrlUserNotification.assignNotification(response[j].user, not)
                            }
                        })
                    }
                    ctrlUserClass.addPositive(userUpdated);
                    if (!userUpdated.confined) ctrlUserClass.addConfined(userUpdated)
                    return res.status(200).send({ message: `Se ha añadido positivo al usuario: ${userUpdated.displayName}` });
                })
                .catch((err) => {
                    return err;
                });
        }
    });
}

function addNegative(req, res) {
    User.findByIdAndUpdate({ _id: req.user }, { infected: false }, (err, userUpdated) => {
        if (err) {
            res
                .status(500)
                .send({ message: "Error al añadir negativo" });
        } else {
            if (!userUpdated.infected) return res.status(400).send({ message: "El usuario: " + userUpdated.displayName + " ya tiene un test negativo" })
            var titulo = "Usuario negativo."
            var description = "El usuario " + userUpdated.displayName + " ha añadido un test negativo."
            var tipo = "NEGATIVO"
            var not = ctrlNotification.createNotification(userUpdated.displayName, titulo, description, tipo)
            ctrlUserSubject.getUserSubjects(req.user).then((response) => {
                    let subjects = [];
                    for (var i = 0; i < response.length; ++i) {
                        subjects.push(response[i].subject)
                    }
                    for (var i = 0; i < subjects.length; ++i) {
                        ctrlUserSubject.getSubjectsTeacher(subjects[i]).then((response) => {
                            for (var j = 0; j < response.length; ++j) {
                                ctrlUserNotification.assignNotification(response[j].user, not)
                            }
                        })
                    }
                    ctrlUserClass.addNegative(userUpdated);
                    return res.status(200).send({ message: `Se ha añadido negativo al usuario: ${userUpdated.displayName}` });
                })
                .catch((err) => {
                    return err;
                });
        }
    });
}

function addConfined(req, res) {
    User.findByIdAndUpdate({ _id: req.user }, { confined: true }, (err, userUpdated) => {
        if (err) {
            res
                .status(500)
                .send({ message: "Error al añadir confinado" });
        } else {
            if (userUpdated.confined) return res.status(400).send({ message: "El usuario: " + userUpdated.displayName + " ya está confinado" })
            var titulo = "Usuario confinado."
            var description = "El usuario " + userUpdated.displayName + " está confinado."
            var tipo = "CONFINADO"
            var not = ctrlNotification.createNotification(userUpdated.displayName, titulo, description, tipo)
            ctrlUserSubject.getUserSubjects(req.user).then((response) => {
                    let subjects = [];
                    for (var i = 0; i < response.length; ++i) {
                        subjects.push(response[i].subject)
                    }
                    for (var i = 0; i < subjects.length; ++i) {
                        ctrlUserSubject.getSubjectsTeacher(subjects[i]).then((response) => {
                            for (var j = 0; j < response.length; ++j) {
                                ctrlUserNotification.assignNotification(response[j].user, not)
                            }
                        })
                    }
                    ctrlUserClass.addConfined(userUpdated);
                    return res.status(200).send({ message: `Se ha añadido confinado al usuario: ${userUpdated.displayName}` });
                })
                .catch((err) => {
                    return err;
                });
        }
    });
}

function addDesconfined(req, res) {
    User.findByIdAndUpdate({ _id: req.user }, { confined: false }, (err, userUpdated) => {
        if (err) {
            res
                .status(500)
                .send({ message: "Error al añadir positivo" });
        } else {
            if (!userUpdated.confined) return res.status(400).send({ message: "El usuario: " + userUpdated.displayName + " no está confinado" })
            if (userUpdated.infected) return res.status(400).send({ message: "El usuario: " + userUpdated.displayName + " tiene un test positivo" })
            var titulo = "Usuario desconfinado."
            var description = "El usuario " + userUpdated.displayName + " se ha desconfinado."
            var tipo = "DESCONFINADO"
            var not = ctrlNotification.createNotification(userUpdated.displayName, titulo, description, tipo)
            ctrlUserSubject.getUserSubjects(req.user).then((response) => {
                    //let subjects = JSON.stringify(response)
                    let subjects = [];
                    for (var i = 0; i < response.length; ++i) {
                        subjects.push(response[i].subject)
                    }
                    for (var i = 0; i < subjects.length; ++i) {
                        ctrlUserSubject.getSubjectsTeacher(subjects[i]).then((response) => {
                            for (var j = 0; j < response.length; ++j) {
                                ctrlUserNotification.assignNotification(response[j].user, not)
                            }
                        })
                    }
                    ctrlUserClass.addDeconfined(userUpdated);
                    return res.status(200).send({ message: `Se ha desconfinado el usuario: ${userUpdated.displayName}` });
                })
                .catch((err) => {
                    return err;
                });
        }
    });
}

function readQR(req, res) {

    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })

        Chair.findOne({ QR: req.body.qr }, (err, chair) => {
            if (err) return res.status(500).send({ message: err })
            if (!chair) return res.status(404).send({ message: "no existe la silla" })
            if (user.role != "ALUMNO")
                return res.status(400).send({
                    message: "El usuario logueado no es un alumno"
                })

            var horainici
            if ((moment().hour() % 2) != 0) horainici = moment().hour() - 1
            else horainici = moment().hour()
            var days = ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY"]
            var today = days[moment().weekday() - 1]
            Schedule.findOne({
                $and: [{
                    h_ini: horainici
                }, {
                    day: today
                }]
            }, (err, schedule) => {
                if (err) return res.status(500).send({ message: err })
                if (!schedule) return res.status(404).send({ message: "no existe el horario" })

                Class.findOne({
                    $and: [{
                        schedule: schedule
                    }, {
                        room: chair.room
                    }]
                }, (err, clas) => {
                    if (err) return res.status(500).send({ message: err })
                    if (!clas) return res.status(404).send({ message: "no existe la clase" })

                    var seat = new Seat({
                        student: user,
                        class: clas,
                        chair: chair,
                        date: Date.now()
                    })

                    seat.save((err) => {
                        if (err) res.status(500).send({
                            message: `Error al asignar el asiento ${err}`
                        })
                        return res.status(201).send({ message: "Se ha asignado el asiento correctamente" })
                    })

                })
            })

        })
    })
}

function readQRBiblio(req, res) {

    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ALUMNO")
            return res.status(400).send({
                message: "El usuario logueado no es un alumno"
            })
        Chair.findOne({ QR: req.body.qr }, (err, chair) => {
            if (err) return res.status(500).send({ message: err })
            if (!chair) return res.status(404).send({ message: "no existe la silla" })
            if (user.role != "ALUMNO")
                return res.status(400).send({
                    message: "El usuario logueado no es un alumno"
                })
            var horainici
            var horafinal
            horainici = moment().hour().toString() + ":" + moment().minutes().toString()
            horafinal = (moment().hour() + 1).toString() + ":" + moment().minutes().toString()

            ctrlBiblioSeat.existsSeat(horainici, moment().format("L"), chair, user).then((response) => {
                if (response) return res.status(500).send({ message: "Este asiento ya ha sido registrado" })

                var seat = new BiblioSeat({
                    h_ini: horainici,
                    h_end: horafinal,
                    date: moment().format("L"),
                    chair: chair,
                    student: user
                })

                ctrlBiblioSeat.updateFinalHour(moment().format("L"), chair, horainici)

                seat.save((err) => {
                    if (err) res.status(500).send({
                        message: `Error al asignar el asiento ${err}`
                    })
                    return res.status(201).send({ message: "Se ha asignado el asiento correctamente" })
                })
            })

        })
    })
}

//ADMINISTRACION

function signInAdmin(req, res) {

    User.findOne({ email: req.body.email }, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ADMINISTRACION")
            return res.status(400).send({
                message: "El usuario logueado no es administrador"
            })

        if (!bcrypt.compareSync(req.body.password, user.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario o contraseña incorrectos"
                }
            });
        }
        req.user = user
        res.status(200).send({
            message: "Te has logueado correctamente",
            token: service.createToken(user),
            displayName: user.displayName,
            role: user.role
        })
    });
}

function addSubject(req, res) {

    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })

        User.findOne({ email: req.body.email }, (err, student) => {
            if (err) return res.status(500).send({ message: err })
            if (!student) return res.status(404).send({ message: "no existe usuario" })
            if (student.role == "ADMINISTRACION") return res.status(400).send({ message: "no se peuden añadir asignaturas a administracion" })
            Subject.findOne(req.name, (err, subject) => {
                if (err) return res.status(500).send({ message: err })
                if (!subject) return res.status(404).send({ message: "no existe asignatura" })
                let studentsubject = new StudentSubject({
                    student: student,
                    subject: subject
                })
                studentsubject.save((err) => {
                    if (err) res.status(500).send({
                        message: `Error al añadir asignatura ${err}`
                    })
                    return res.status(201).send({ message: "Se ha añadido la asignatura correctamente" })
                })
            })
        })
    })
}


function addRoom(req, res) {
    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })

        var classroom = new Room({
            floor: req.body.floor,
            number: req.body.number,
            capacity: req.body.capacity,
            closed: req.body.closed,
            building: req.body.building,
            available: req.body.available,
            pcs: req.body.pcs
        })
        var chairError = chairController.createChair(classroom)
        if (chairError) res.status(500).send({
            message: `Error al añadir la silla ${err3}`
        })
        classroom.save((err) => {
            if (err) res.status(500).send({
                message: `Error al añadir el aula ${err3}`
            })
            return res.status(201).send({ message: "Se ha añadido el aula correctamente" })
        })
    })
}

function addSchedule(req, res) {
    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })

        var schedule = new Schedule({
            h_ini: req.body.h_ini,
            h_end: req.body.h_end,
            day: req.body.day
        })

        schedule.save((err) => {
            if (err) res.status(500).send({
                message: `Error al añadir el horario ${err}`
            })
            return res.status(201).send({ message: "Se ha añadido el horario correctamente" })
        })

    })
}

function addClass(req, res) {

    User.findById(req.user, (err, admin) => {

        if (err) return res.status(500).send({ message: err })

        if (!admin) return res.status(404).send({ message: "no existe usuario" })
        if (admin.role != "ADMINISTRACION") return res.status(403).send({ message: "No tienes permisos" })

        User.findOne({ email: req.body.email }, (err, prof) => {
            if (err) return res.status(500).send({ message: err })
            if (!prof) return res.status(404).send({ message: "no existe usuario" })
            if (prof.role != "PROFESOR") return res.status(400).send({ message: "Sólo le puedes asignar una clas a un profesor" })
            Subject.findOne({
                $and: [{
                    code: req.body.code
                }, {
                    name: req.body.name
                }]
            }, (err, subject) => {
                if (err) return res.status(500).send({ message: err })
                if (!subject) return res.status(404).send({ message: "no existe asignatura" })

                Schedule.findOne({
                    $and: [{
                        h_ini: req.body.h_ini
                    }, {
                        day: req.body.weekday
                    }]
                }, (err, schedule) => {
                    if (err) return res.status(500).send({ message: err })
                    if (!schedule) return res.status(404).send({ message: "no existe asignatura" })

                    Room.findOne({
                        $and: [{
                            floor: req.body.floor
                        }, {
                            number: req.body.number
                        }, {
                            building: req.body.building
                        }]
                    }, (err, room) => {

                        if (err) return res.status(500).send({ message: err })
                        if (!room) return res.status(404).send({ message: "no existe asignatura" })
                        if (req.body.building == "BIBLIOTECA") return res.status(403).send({ message: "No le puedes añadir una clase a la bibloteca" })

                        let clas = new Class({
                            group: req.body.group,
                            teacher: prof,
                            room: room,
                            schedule: schedule,
                            subject: subject,
                            num_students: 0,
                            num_infected: 0,
                            num_confined: 0
                        })

                        clas.save((err) => {
                            if (err) res.status(500).send({
                                message: `Error al añadir la clase ${err}`
                            })
                            return res.status(201).send({ message: "Se ha añadido la clase correctamente" })
                        })
                    })
                })
            })
        })
    })
}


function myClass(req, res) {
    //get de mis clases

}

function getSubjectsfromUser(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role == "ADMINISTRACION" || user.role == "PROFESOR") {
            ctrlUserSubject.getUserSubjects(req.params.student).then((response) => {
                if (err) res.status(500).send({ message: `${err}` })
                else if (response.length == 0) res.status(200).send({ 'subjs': [] })
                else {
                    var subjs = []
                    var j = 0
                    for (var i = 0; i < response.length; ++i) {
                        ctrlSubject.getSubject(response[i].subject).then((resul) => {
                            subjs.push(resul[0])
                                ++j
                            if (j == response.length)
                                res.status(200).send({ subjs })
                        })
                    }
                }
            })
        } else return res.status(403).send({ message: "No tienes permisos" })
    })
}

function user(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        res.status(200).send({ message: user })
    })
}


function getTeacher(teacher) {
    return db.collection("users").find({ _id: ObjectId(teacher) }, { projection: { displayName: 1, _id: 0 } }).toArray()
}

function getUserClasses(req, res) {
    var userclases = []
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ALUMNO") return res.status(403).send({ message: "No tienes permisos" })
        userclass.find({ user: ObjectId(req.user) }, (err, classe) => {
            if (err) return res.status(500).send({ message: err })
            if (!classe) return res.status(404).send({ message: "no tiene clases" })
            var j = 0
            for (var i = 0; i < classe.length; ++i) {
                Class.findById(classe[i].class, (err, clases) => {
                    if (err) return res.status(500).send({ message: err })
                    if (!clases) return res.status(404).send({ message: "no existe la clase" })

                    Subject.findById(clases.subject, (err, subj) => {
                        if (err) return res.status(500).send({ message: err })
                        if (!subj) return res.status(404).send({ message: "no tiene asiganturas" })

                        Room.findById(clases.room, (err, room) => {
                            if (err) return res.status(500).send({ message: err })
                            if (!room) return res.status(404).send({ message: "no tiene room" })
                            Schedule.findById(clases.schedule, (err, schedule) => {
                                User.findById(clases.teacher, (err, teacher) => {
                                    var edificio;
                                    if (room.number < 10)
                                        edificio = room.building + room.floor.toString() + '0' + room.number.toString()
                                    else edificio = room.building + room.floor.toString() + room.number.toString()
                                    let classeUser = {
                                        subject: subj.name,
                                        room: edificio,
                                        weekday: schedule.day,
                                        h_ini: schedule.h_ini,
                                        h_end: schedule.h_end,
                                        teacher: teacher.displayName,
                                        mail_teacher: teacher.email,
                                        num_infected: clases.num_infected,
                                        attendance: subj.attendance
                                    }
                                    userclases.push(classeUser)
                                        ++j
                                    if (j == classe.length) return res.status(200).send(userclases)
                                })
                            })
                        })
                    })
                })
            }
        })
    })
}

module.exports = {
    signUp,
    signIn,
    signInAdmin,
    home,
    addPositive,
    addNegative,
    addConfined,
    addDesconfined,
    user,
    getUser,
    getUsers,
    addRoom,
    addSchedule,
    addClass,
    readQR,
    readQRBiblio,
    myClass,
    addSubject,
    getTeacher,
    getUserClasses,
    getSubjectsfromUser
}