'use strict'

const BiblioSeat = require('../models/biblioseat');
const User = require('../models/user');
const { ObjectId } = require('mongodb');
const { db } = require('../models/biblioseat');
const moment = require('moment')

function getBiblioSeat(today, chair) {
    return db.collection("biblioseats")
        .find({ chair: ObjectId(chair._id), date: today }, { projection: { h_ini: 1, h_end: 1, _id: 1 } })
        .toArray()
}


function getBiblioSeatFromChair(chair) {
    return db.collection("biblioseats")
        .find({ chair: ObjectId(chair._id) }, { projection: { h_ini: 1, h_end: 1, _id: 1 } })
        .toArray()
}

function findAndUpdate(id, hour) {
    BiblioSeat.findByIdAndUpdate({ _id: id }, { h_end: hour }, (err, seatUpdated) => {
        if (err) return res.status(500).send({ message: "Error al actualizar hora de finalización de la silla" });
    })
}

function getBiblioSeatWithAllParams(day, chair, user) {
    return db.collection("biblioseats")
        .find({ chair: ObjectId(chair._id), date: day, student: user._id }, { projection: { h_ini: 1, h_end: 1, _id: 1 } })
        .toArray()
}

function updateFinalHour(today, chair, hour) {
    getBiblioSeat(today, chair).then((response) => {

        for (var i = 0; i < response.length; ++i) {
            var h_ini_split = response[i].h_ini.split(":")
            var h_end_split = response[i].h_end.split(":")

            var horainici = parseInt(h_ini_split[0])
            var minutsinici = parseInt(h_ini_split[1])
            var horaend = parseInt(h_end_split[0])
            var minutsend = parseInt(h_end_split[1])

            var actual_hour_split = hour.split(":")
            var actual_hour = parseInt(actual_hour_split[0])
            var actual_minut = parseInt(actual_hour_split[1])

            if (horainici == actual_hour && minutsinici < actual_minut) {
                findAndUpdate(response[i]._id, hour)
            } else if (horainici < actual_hour && horaend == actual_hour) {
                if (minutsend > actual_minut) {

                    findAndUpdate(response[i]._id, hour)
                }
            }
        }
    })
}

function isAvailable(chair) {
    return getBiblioSeatFromChair(chair).then((response) => {
        var hour = moment().hour()
        var minutes = moment().minutes()
        for (var i = 0; i < response.length; ++i) {
            var h_ini_split = response[i].h_ini.split(":")
            var h_end_split = response[i].h_end.split(":")

            var horainici = parseInt(h_ini_split[0])
            var minutsinici = parseInt(h_ini_split[1])
            var horaend = parseInt(h_end_split[0])
            var minutsend = parseInt(h_end_split[1])

            if (horainici == hour && minutsinici < minutes) {
                return false
            } else if (horainici < hour && horaend == hour) {
                if (minutsend > minutes) {
                    return false
                }
            }
        }
        return true
    })
}

function existsSeat(hour, day, chair, user) {
    return getBiblioSeatWithAllParams(day, chair, user._id).then((response) => {
        if (response.length != 0) {
            for (var i = 0; i < response.length; ++i) {
                var h_ini_split = response[i].h_ini.split(":")
                var h_end_split = response[i].h_end.split(":")

                var horainici = parseInt(h_ini_split[0])
                var minutsinici = parseInt(h_ini_split[1])
                var horaend = parseInt(h_end_split[0])
                var minutsend = parseInt(h_end_split[1])

                var actual_hour_split = hour.split(":")
                var actual_hour = parseInt(actual_hour_split[0])
                var actual_minut = parseInt(actual_hour_split[1])

                if (horainici == actual_hour && minutsinici < actual_minut) {
                    return true
                } else if (horainici < actual_hour && horaend == actual_hour) {
                    if (minutsend > actual_minut) {
                        return true
                    }
                }
            }
        } else return false
    })
}

function getSeatHistoryBiblio(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ALUMNO") {
            User.findOne({ email: req.query.email }, (err, student) => {
                if (err) return res.status(500).send({ message: err })
                if (!student) return res.status(404).send({ message: "no existe usuario" })
                if (student.role == "ALUMNO") {
                    BiblioSeat.find({ student: student._id }).select('chair h_ini h_end date').populate([{
                        path: 'chair',
                        model: 'Chair',
                        select: 'QR -_id',
                        populate: [{
                            path: 'room',
                            model: 'Room',
                            select: 'building floor number -_id'
                        }]
                    }]).then((seats) => {
                        return res.status(200).send(seats);
                    })
                } else {
                    return res.status(404).send({ message: "el usuario no es un alumno" })
                }
            })
        }
    })
}

module.exports = {
    updateFinalHour,
    isAvailable,
    existsSeat,
    getSeatHistoryBiblio
}