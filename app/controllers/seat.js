'use strict'

const User = require('../models/user');
let SeatModel = require("../models/seat.js");

function getSeatHistory(req, res) {
    User.findById(req.user, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: "no existe usuario" })
        if (user.role != "ALUMNO"){
            User.findOne({ email: req.query.email }, (err, student) => {
                if (err) return res.status(500).send({ message: err })
                if (!student) return res.status(404).send({ message: "no existe usuario" })
                if (student.role == "ALUMNO"){
                    SeatModel.find({ student: student._id }).select('class chair date').populate([
                        {
                            path: 'chair',
                            model: 'Chair',
                            select: 'QR -_id'
                        },
                        {
                            path: 'class',
                            model: 'Class',
                            select: 'teacher -_id',
                            populate: [
                                {
                                    path: 'teacher',
                                    model: 'User',
                                    select: 'displayName -_id',
                                }, {
                                    path: 'subject',
                                    model: 'Subject',
                                    select: 'name -_id',
                                }, {
                                    path: 'room',
                                    model: 'Room',
                                    select: 'building floor number -_id'
                                }, {
                                    path: 'schedule',
                                    model: 'Schedule',
                                    select: 'h_ini h_end day -_id'
                                }
                            ]
                        }
                    ]).then((seats) => {
                        return res.status(200).send(seats);
                    })
                } else {
                    return res.status(404).send({ message: "el usuario no es un alumno" })
                }
            })
        }
    })
}

module.exports = {
    getSeatHistory
}