'use strict'

const express = require('express');
const api = express.Router();

//Controllers
const UserCtrl = require('../controllers/user')
const NotificationCtrl = require('../controllers/notification')
const SubjectCtrl = require('../controllers/subject')
const auth = require('../middlewares/auth');
const UserSubjectCtrl = require('../controllers/usersubject');
const UserNotificationCtrl = require('../controllers/usernotification');
const RoomCtrl = require('../controllers/room')
const SeatCtrl = require('../controllers/seat')
const BiblioSeatCtrl = require('../controllers/biblioseat')
const ClassCtrl = require('../controllers/class')
const SchoolCtrl = require('../controllers/school')
const TwitterCtrl = require('../controllers/twitter')

//Routes
api.post('/signup', auth, UserCtrl.signUp);
api.post('/signin', UserCtrl.signIn);
api.post('/signinadmin', UserCtrl.signInAdmin);

api.get('/home', auth, UserCtrl.home);

api.put('/home/addpositive', auth, UserCtrl.addPositive);
api.put('/home/addnegative', auth, UserCtrl.addNegative);
api.put('/home/addconfined', auth, UserCtrl.addConfined);
api.put('/home/adddesconfined', auth, UserCtrl.addDesconfined);
api.get('/getuser', auth, UserCtrl.user);
api.get('/getuserclasses', auth, UserCtrl.getUserClasses);
api.get('/user/:id', auth, UserCtrl.getUser);
api.get('/users', auth, UserCtrl.getUsers);

api.get('/notifications', auth, NotificationCtrl.getNotifications);
api.get('/notifications/:type', auth, NotificationCtrl.getNotificationsFiltred);


api.get('/mynotifications', auth, UserNotificationCtrl.getUserNotifications);
api.post('/addsubject', auth, SubjectCtrl.addSubject);
api.post('/createsubject', auth, SubjectCtrl.createSubject);
api.get('/subjects', auth, UserSubjectCtrl.getUserSubjects);
api.get('/subjectsteacher', auth, UserSubjectCtrl.getTeacherSubjects);

api.put('/subject/assistanceonline/:subject', auth, UserNotificationCtrl.changeAssistanceTypeOnline);
api.put('/subject/assistance/:subject', auth, UserNotificationCtrl.changeAssistanceTypeF2F);
api.put('/subject/docentguide/:subject', auth, UserNotificationCtrl.changeDocentGuide);
api.get('/subjectsuser/:student', auth, UserCtrl.getSubjectsfromUser);



api.get('/rooms', auth, RoomCtrl.getRooms);
api.get('/availablerooms', auth, RoomCtrl.getAvailableRooms);
api.get('/seathistory', auth, SeatCtrl.getSeatHistory);
api.get('/seathistorybiblio', auth, BiblioSeatCtrl.getSeatHistoryBiblio);
api.get('/availablechairs', auth, RoomCtrl.consultFreeChairs);

api.put('/home/readqr', auth, UserCtrl.readQR);
api.put('/home/readqrbiblio', auth, UserCtrl.readQRBiblio);


//api.post('/home/addhygiene', auth, userCtrl.addHygiene);

api.post('/addroom', auth, UserCtrl.addRoom);
api.post('/addschedule', auth, UserCtrl.addSchedule);
api.post('/addclass', auth, UserCtrl.addClass);
api.post('/adduserclass', auth, ClassCtrl.addUserClass);
api.put('/closeroom', auth, RoomCtrl.closeRoom);
api.put('/openroom', auth, RoomCtrl.openRoom);

api.post('/addnews', auth, NotificationCtrl.createNews);

api.get('/confineds/:id', auth, ClassCtrl.confined);
api.get('/classes', auth, ClassCtrl.getClasses);

api.post('/addsecuritymeasures', auth, NotificationCtrl.createSecurityMeasures);
api.post('/addincidences', auth, NotificationCtrl.addIncidences);
api.post('/addregulations', auth, NotificationCtrl.addRegulations);
api.get('/getschool/:name', auth, SchoolCtrl.getSchool);
api.post('/closeschool/:name', auth, SchoolCtrl.closeSchool);
api.post('/openschool/:name', auth, SchoolCtrl.openSchool);
api.delete('/resolveincidence/:id', auth, NotificationCtrl.resolveIncidences)

//Twitter
api.post('/tweet', auth, TwitterCtrl.tweet);

module.exports = api