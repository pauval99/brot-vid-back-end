'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let validRooms = {
    values: ["BIBLIOTECA", "A4", "A5", "A6"],
    message: '{VALUE} no es un aula válida'
}

let RoomSchema = new Schema({
    floor: { type: Number, required: [true] }, //0
    number: { type: Number, required: [true] }, //01
    capacity: { type: Number, required: [true] },
    closed: { type: Boolean, required: [true] },
    building: {
        type: String,
        required: [true],
        enum: validRooms,
    }, //A6
    available: { type: Boolean },
    pcs: { type: Boolean }
})

module.exports = mongoose.model('Room', RoomSchema)