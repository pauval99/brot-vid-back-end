'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SchoolSchema = new Schema({
    name: { type: String, required: [true] },
    opened: { type: Boolean, required: [true] }
})

module.exports = mongoose.model('School', SchoolSchema)