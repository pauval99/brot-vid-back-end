'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let typeNotification = {
    values: ["GENERAL", "AUTOMATICA", "OTRO", "POSITIVO", "NEGATIVO", "CONFINADO", "DESCONFINADO", "NOTICIAS", "SEGURIDAD", "NORMATIVA", "INCIDENCIA", "ASISTENCIA", "GUIA"],
    message: '{VALUE} no es un tipo de notificacion válida'
}

let NotificationSchema = new Schema({
    creator: { type: String, required: [true] },
    creationDate: { type: Date, required: [true] },
    title: { type: String, required: [true] },
    description: { type: String, required: [true] },
    type: {
        type: String,
        required: [true],
        enum: typeNotification,
    },
    site: {
        type: String
    }
})

module.exports = mongoose.model('Notification', NotificationSchema)