'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSubjectSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    subject: { type: Schema.Types.ObjectId, ref: 'Subject' },
    teacher: { type: Boolean, default: false }
})

module.exports = mongoose.model('UserSubject', UserSubjectSchema)