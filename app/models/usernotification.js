'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserNotificationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    notification: { type: Schema.Types.ObjectId, ref: 'Notification' }
})

module.exports = mongoose.model('UserNotification', UserNotificationSchema)