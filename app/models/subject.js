'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SubjectSchema = new Schema({
    code: { type: Number, required: [true] }, //0
    name: { type: String, required: [true] }, //01
    attendance: { type: Boolean, default: false }
})

module.exports = mongoose.model('Subject', SubjectSchema)