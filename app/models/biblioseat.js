'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BiblioSeatSchema = new Schema({
    h_ini: { type: String, required: [true] }, //0
    h_end: { type: String, required: [true] }, //01
    //available: { type: Boolean, required: [true] }, 
    date: { type: String, required: [true]},
    chair: { type: Schema.Types.ObjectId, ref: 'Chair' },
    student: { type: Schema.Types.ObjectId, ref: 'User' }
})

module.exports = mongoose.model('BiblioSeat', BiblioSeatSchema)