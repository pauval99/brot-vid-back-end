'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserClassSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    class: { type: Schema.Types.ObjectId, ref: 'Class' }
})

module.exports = mongoose.model('UserClass', UserClassSchema)