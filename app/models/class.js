'use strict'

const mongoose = require('mongoose');
const usrCtrl = require('../controllers/user');
const Schema = mongoose.Schema;

let ClassSchema = new Schema({
    group: { type: Number, required: [true] },
    teacher: { type: Schema.Types.ObjectId, ref: 'User' },
    room: { type: Schema.Types.ObjectId, ref: 'Room' },
    schedule: { type: Schema.Types.ObjectId, ref: 'Schedule' },
    subject: { type: Schema.Types.ObjectId, ref: 'Subject' },
    num_students: { type: Number},
    num_infected: { type: Number },
    num_confined: { type: Number }
})

module.exports = mongoose.model('Class', ClassSchema)