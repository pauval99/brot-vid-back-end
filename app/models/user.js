'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let rolesValidos = {
    values: ["ALUMNO", "PROFESOR", "ADMINISTRACION"],
    message: '{VALUE} no es un rol válido'
}

let UserSchema = new Schema({
    email: { type: String, unique: true, lowercase: true, required: [true] },
    displayName: {
        type: String,
        required: [true]
    },
    password: { type: String, required: [true] },
    confined: {
        type: Boolean,
        default: false
    },
    infected: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        required: [true],
        enum: rolesValidos,
    },
    signupDate: { type: Date, default: Date.now() },
    lastLogin: Date
})


module.exports = mongoose.model('User', UserSchema)