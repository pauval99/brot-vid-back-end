'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let weekday = {
    values: ["MONDAY", "THURSDAY", "WEDNESDAY", "TUESDAY", "FRIDAY"],
    message: '{VALUE} no es un tipo de día válido'
}

let ScheduleSchema = new Schema({
    h_ini: { type: Number, required: [true] }, //0
    h_end: { type: Number, required: [true] }, //01
    day: {
        type: String,
        required: [true],
        enum: weekday,
    },
})

module.exports = mongoose.model('Schedule', ScheduleSchema)