'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ChairSchema = new Schema({
    QR: { type: String},
    floor: { type: Number},
    number: { type: Number},
    room: { type: Schema.Types.ObjectId, ref: 'Room'}
})

module.exports = mongoose.model('Chair', ChairSchema)