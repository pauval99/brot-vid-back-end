'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SeatSchema = new Schema({
    student: { type: Schema.Types.ObjectId, ref: 'User' },
    class: { type: Schema.Types.ObjectId, ref: 'Class' },
    chair: { type: Schema.Types.ObjectId, ref: 'Chair' },
    date: { type: Date}
})

module.exports = mongoose.model('Seat', SeatSchema)