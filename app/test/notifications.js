process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

const mongoose = require('mongoose')
const config = require('../config')

const User = require('../models/user')
const bcrypt = require('bcrypt')

const API = 'localhost:3000/api'

chai.use(chaiHttp);

before(function (done) {   
    mongoose.connect(config.db, function(){
        mongoose.connection.db.dropDatabase(function(){
            done()
        })    
    })
})


describe('Notifications', () => {
    it('Addpositive sin permisos', (done) => {
        chai.request(API)
        .put('/home/addpositive')
        .end((err, res) => {
            res.should.have.status(403);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('No tienes autorizacion');
            done();
        });
    });
    it('Addnegative sin permisos', (done) => {
        chai.request(API)
        .put('/home/addnegative')
        .end((err, res) => {
            res.should.have.status(403);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('No tienes autorizacion');
            done();
        });
    });
    it('Test addnegative correcto', (done) => {
        let token;
        chai.request(API)
            .post('/signinAdmin')
            .send({
                email: 'email@email.com',
                password: 'password'
            })
            .end((err, res) => {
                res.body.should.have.property('message').eql('Te has logueado correctamente');
                token = res.body.token;
            done();
        });

        chai.request(API)
        .put('/home/addnegative')
        .set('authorization', 'Beaver ' + token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('Se ha añadido negativo al usuario: admin');
            done();
        });
        });

    it('Test addpositive correcto', (done) => {
        let token;
        chai.request(API)
            .post('/signinAdmin')
            .send({
                email: 'email@email.com',
                password: 'password'
            })
            .end((err, res) => {
                res.body.should.have.property('message').eql('Te has logueado correctamente');
                token = res.body.token;
            done();
        });

        chai.request(API)
        .put('/home/addpositive')
        .set('authorization', 'Beaver ' + token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('Se ha añadido positivo al usuario: admin');
            done();
        });
     });
     it('Test notifications negative', (done) => {
        let token;
        chai.request(API)
            .post('/signinAdmin')
            .send({
                email: 'email@email.com',
                password: 'password'
            })
            .end((err, res) => {
                res.body.should.have.property('message').eql('Te has logueado correctamente');
                token = res.body.token;
            done();
        });

        chai.request(API)
        .put('/home/addnegative')
        .set('authorization', 'Beaver ' + token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('Se ha añadido negativo al usuario: admin');
            done();
        });
        chai.request(API)
        .get('/notifications/negative')
        .set('authorization', 'Beaver ' + token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('title');
            res.body.should.have.property('title').eql('El usuario amdin ha añadido un test negativo.');
            done();
        });
        });

        it('Test notifications positive', (done) => {
            let token;
            chai.request(API)
                .post('/signinAdmin')
                .send({
                    email: 'email@email.com',
                    password: 'password'
                })
                .end((err, res) => {
                    res.body.should.have.property('message').eql('Te has logueado correctamente');
                    token = res.body.token;
                done();
            });
    
            chai.request(API)
            .put('/home/addpositive')
            .set('authorization', 'Beaver ' + token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('message').eql('Se ha añadido positivo al usuario: admin');
                done();
            });
            chai.request(API)
            .get('/notifications/positive')
            .set('authorization', 'Beaver ' + token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('title');
                res.body.should.have.property('title').eql('El usuario amdin ha añadido un test positivo.');
                done();
            });
            });
            it('Test addconfined ', (done) => {
                let token;
                chai.request(API)
                    .post('/signinAdmin')
                    .send({
                        email: 'email@email.com',
                        password: 'password'
                    })
                    .end((err, res) => {
                        res.body.should.have.property('message').eql('Te has logueado correctamente');
                        token = res.body.token;
                    done();
                });
        
                chai.request(API)
                .put('/home/addconfined')
                .set('authorization', 'Beaver ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('Se ha añadido confinado al usuario: admin');
                    done();
                });
             });
             it('Test adddesconfined ', (done) => {
                let token;
                chai.request(API)
                    .post('/signinAdmin')
                    .send({
                        email: 'email@email.com',
                        password: 'password'
                    })
                    .end((err, res) => {
                        res.body.should.have.property('message').eql('Te has logueado correctamente');
                        token = res.body.token;
                    done();
                });
        
                chai.request(API)
                .put('/home/adddesconfined')
                .set('authorization', 'Beaver ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('El usuario: admin no está confinado');
                    done();
                });
             });
             it('Mynotifications', (done) => {
                let token;
                chai.request(API)
                    .post('/signinAdmin')
                    .send({
                        email: 'email@email.com',
                        password: 'password'
                    })
                    .end((err, res) => {
                        res.body.should.have.property('message').eql('Te has logueado correctamente');
                        token = res.body.token;
                    done();
                });
        
                chai.request(API)
                .get('/mynotifications')
                .set('authorization', 'Beaver ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('No hay notificaciones que mostrar');
                    done();
                });
                });
    });