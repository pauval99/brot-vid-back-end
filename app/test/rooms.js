process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

const mongoose = require('mongoose')
const config = require('../config')

const User = require('../models/user')
const bcrypt = require('bcrypt')

const API = 'localhost:3000/api'

chai.use(chaiHttp);

before(function (done) {   
    mongoose.connect(config.db, function(){
        mongoose.connection.db.dropDatabase(function(){
            done()
        })    
    })
})


describe('Rooms managment', () => {
    it('Create user alumno', (done) => {
        let user = new User({
            email: 'test@email.com',
            displayName: 'alumno',
            password: bcrypt.hashSync('password', 10),
            role: 'ALUMNO',
            confined: false,
            infected: false,
        })
        user.save((err) => {
            if (err) console.log(`Error al crear el usuario ${err}`);
        });
        done();
    });

    it('Read QR', (done) => {
        let token;
        chai.request(API)
            .post('/signinAdmin')
            .send({
                email: 'email@email.com',
                password: 'password'
            })
            .end((err, res) => {
                res.body.should.have.property('message').eql('Te has logueado correctamente');
                token = res.body.token;
            done();
        });

        chai.request(API)
        .put('/home/readQR')
        .set('authorization', 'Beaver ' + token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('no existe la silla');
            done();
        });
        });

        });
