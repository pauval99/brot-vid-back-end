process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

const mongoose = require('mongoose')
const config = require('../config')

const User = require('../models/user')
const bcrypt = require('bcrypt')

const API = 'localhost:3000/api'

chai.use(chaiHttp);

before(function (done) {   
    mongoose.connect(config.db, function(){
        mongoose.connection.db.dropDatabase(function(){
            done()
        })    
    })
})

describe('Create user', () => {
    it('Create user', (done) => {
        let user = new User({
            email: 'email@email.com',
            displayName: 'admin',
            password: bcrypt.hashSync('password', 10),
            role: 'ADMINISTRACION',
            confined: false,
            infected: false,
        })
        user.save((err) => {
            if (err) console.log(`Error al crear el usuario ${err}`);
        });
        done();
    });
});

describe('Login', () => {

    it('Test login admin', (done) => {
        let user = new User({
            email: 'email@email.com',
            displayName: 'admin',
            password: bcrypt.hashSync('password', 10),
            role: 'ADMINISTRACION',
            confined: false,
            infected: false,
        })
        user.save((err) => {
            if (err) console.log(`Error al crear el usuario ${err}`);
        });
        chai.request(API)
            .post('/signinAdmin')
            .send({
                email: 'email@email.com',
                password: 'password'
            })
            .end((err, res) => {
                res.body.should.have.property('message').eql('Te has logueado correctamente');
            done();
        });
    });
    it('Test login fallido', (done) => {
        chai.request(API)
        .post('/signin')
        .send({
            email: 'notworking@test.com',
            password: 'password'
        })
        .end((err, res) => {
            res.should.have.status(404);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('message').eql('no existe usuario');
            done();
        });
    });
});
