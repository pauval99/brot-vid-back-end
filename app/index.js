'use strict'

const mongoose = require('mongoose')
const app = require('./app')
const config = require('./config')
const process = require('process');

mongoose.connect(config.db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}, (err, res) => {
    if (err) {
        return console.log(`Error al conectar a la db ${err}`)
    }
    console.log('conexion establecida')
})
app.listen(config.port, () => {
    console.log(`api rest corriendo en ${config.port}`)
})

process.on('SIGINT', function() {
    process.exit()
})