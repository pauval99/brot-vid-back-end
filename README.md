## Users

 + **email:** admin@admin.com
 + **password:** admin
 
 + **email:** alumno1@gmail.com
 + **password:** alumno
 
 + **email:** alumno3@gmail.com
 + **password:** alumno
 
 + **email:** alumno5@gmail.com
 + **password:** alumno
 
 + **email:** alumno7@gmail.com
 + **password:** alumno
 
 + **email:** alumno9@gmail.com
 + **password:** alumno
 
 + **email:** profesor1@gmail.com
 + **password:** profesor
 
 + **email:** profesor3@gmail.com
 + **password:** profesor
 
 + **email:** profesor5@gmail.com
 + **password:** profesor
 

## Run server
+ npm i -S nodemon
+ npm start //localhost:3000

## Run tests
+ docker-compose up -d
+ docker exec -it brot-vid_node npm run test

## API
+ **URL:** 35.180.26.95:3000/api
+ **SSH:** ssh ec2-user@35.180.26.95

## SonarCloud
+ [SonarCloud](https://sonarcloud.io/dashboard?id=pauval99_brot-vid-back-end)

## MongoDB Atlas
+ [Atlas](https://cloud.mongodb.com/v2/5f88cc54d168ad7bc098b23d#clusters)