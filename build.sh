#!/bin/bash

docker build -f Dockerfile -t pauval99/brot-vid_back-end:$1 .
docker build -f Dockerfile -t pauval99/brot-vid_back-end:latest .

docker push pauval99/brot-vid_back-end:$1
docker push pauval99/brot-vid_back-end:latest

docker rmi pauval99/brot-vid_back-end:$1
docker rmi pauval99/brot-vid_back-end:latest