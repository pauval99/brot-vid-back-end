FROM node:14.14

USER node

COPY --chown=node:node ./app /home/node/app

WORKDIR /home/node/app

RUN npm install

CMD [ "npm", "start" ]